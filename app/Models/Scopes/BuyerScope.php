<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class BuyerScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     * In Laravel's Eloquent ORM,
     * a global scope is a way to define query constraints
     * that are automatically applied to all queries performed on a particular model.
     * Global scopes allow you to define common conditions or filters that should be applied to queries for a specific model,
     * reducing the need to repeat the same constraints in multiple queries
     */
    public function apply(Builder $builder, Model $model): void
    {
        $builder->has('transactions');
    }
}
