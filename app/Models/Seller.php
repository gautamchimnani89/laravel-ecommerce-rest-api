<?php

namespace App\Models;

use App\Models\Scopes\SellerScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

// seller is user then why should we make seperate table we will use inherit

class Seller extends User {
    use HasFactory;

    protected $table = 'users';

    protected static function boot() {
        parent::boot();
        static::addGlobalScope(new SellerScope());
    }

    public function products() {
        return $this->hasMany(Product::class);
    }
}
