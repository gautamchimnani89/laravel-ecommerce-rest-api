<?php

namespace App\Models;

use App\Models\Scopes\BuyerScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

// buyer is user then why should we make seperate table we will use inherit

class Buyer extends User {
    use HasFactory;

    protected $table = 'users';


    protected static function boot() {
        parent::boot();
        static::addGlobalScope(new BuyerScope());
    }

    public function transactions() {
        return $this->hasMany(Transaction::class);
    }
}
