<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Seller;
use Illuminate\Http\Request;

class SellerCategoriesController extends ApiController {
    /**
     * whereHas('categories') method ensures that only products with associated categories are considered
     * The pluck('categories') method extracts the categories attribute from each product.
     */
    public function index(Seller $seller) {
        $categories = $seller->products()
            ->whereHas('categories')
            ->get()
            ->pluck('categories')
            ->flatten()
            ->unique()
            ->values();

        return $this->showAll($categories);
    }
}
