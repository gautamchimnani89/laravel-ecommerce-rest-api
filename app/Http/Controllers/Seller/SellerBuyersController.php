<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Seller;
use Illuminate\Http\Request;

class SellerBuyersController extends ApiController
{
    /**
     * whereHas('transactions') method ensures that only products with associated transactions are considered
     * with('transactions.buyer') method eager loads and retrieve the buyer information. 
     */
    public function index(Seller $seller)
    {
        $buyers = $seller->products()
                        ->whereHas('transactions')
                        ->with('transactions.buyer')
                        ->get()
                        ->pluck('transactions')
                        ->flatten()
                        ->pluck('buyer')
                        ->unique()
                        ->values();

        return $this->showAll($buyers);
    }
}
