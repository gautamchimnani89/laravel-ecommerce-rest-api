<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryBuyersController extends ApiController
{
    public function index(Category $category)
    {
        /**
         * whereHas('transactions') method ensures that only products with associated transactions are considered.
         * with('transactions.buyer') method eager loads and retrieve the buyer information.
         */

        $buyers = $category->products()
            ->whereHas('transactions')
            ->with('transactions.buyer')
            ->get()
            ->pluck('transactions')
            ->flatten()
            ->pluck('buyer')
            ->unique()
            ->values();

        return $this->showAll($buyers);
    }
}