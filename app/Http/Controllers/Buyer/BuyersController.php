<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Buyer;
use Illuminate\Http\Request;

class BuyersController extends ApiController {
    /**
     * Display a listing of the resource.
     */
    public function index() {
        $buyers = Buyer::has('transactions')->get();

        return $this->showAll($buyers , 200);
    }
    
    /**
     * Display the specified resource.
     */
    public function show(Buyer $buyer) {
        // $buyer = Buyer::has('transactions')->findOrFail($id);
        
        return $this->showOne($buyer , 200);
    }
}
