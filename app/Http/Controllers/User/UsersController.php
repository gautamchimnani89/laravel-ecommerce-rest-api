<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class UsersController extends ApiController {
    /**
     * Display a listing of the resource.
     */
    public function index() {
        $users = User::all();
        return $this->showAll($users);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request) {
        $rules = [
            'name' => 'required',
            'password' => 'required|min:8|max:255|confirmed',
            'email' => 'required|email|unique:users'
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['password'] = bcrypt($request->password);
        $data['verified'] = User::UNVERIFIED_USER;
        $data['verification_token'] = User::generateVerificationToken();
        $data['admin'] = User::REGULAR_USER;


        $user = User::create($data);

        return $this->showOne($user , 201);
    }
    
    /**
     * Display the specified resource.
     */
    public function show(User $user) {
        return $this->showOne($user , 201);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user) {
        $rules = [
            'email' => 'email|unique:users,email,' . $user->id,
            'password' => 'min:8|max:255|confirmed',
            'admin' => 'in:' . User::REGULAR_USER . ',' . User::ADMIN_USER,
        ];
        $this->validate($request, $rules);
        
        if ($request->has('name')) {
            $user->name = $request->name;
        }

        if ($request->has('email')) {
            $user->email = $request->email;
            $user->verified = User::UNVERIFIED_USER;
            $user->verification_token = User::generateVerificationToken();
        }

        if ($request->has('password')) {
            $user->password = bcrypt($request->password);
        }

        if ($request->has('admin')) {
            if (!$user->isVerified()) {
                return $this->errorResponse('verified users can modify the admin field', 409);
            }

            $user->admin = $request->admin;
        }

        /**
         * When called on an instance of an Eloquent model, the isDirty() method examines the model's attributes and checks if any of them have been changed. It returns true if there are any modified attributes, and false otherwise.
         */
        if (!$user->isDirty()) {
            /**
             * ==> UPDATE
             * AT THE TIME OF UPDATE ,RECORD WILL UPDATE AGAIN & AGAIN
             * BECAUSE TOKEN IS GENERATING AGAIN AND AGAIN
             */
            return response()->json(['error' => 'You need to specify a different value!'], 422);
        }


        $user->save();

        return $this->showOne($user , 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user) {
        $user->delete();

        return $this->showOne(new User() , 204);
    }


    public function verify(string $token) {
        $user = User::where('verification_token', $token)->firstOrFail();
        
        $user->verified = User::VERIFIED_USER;

        $user->verification_token = null;

        $user->save();

        return $this->showMessage("The account has been verified");
    }
}

