<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\ApiController;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductCategoriesController extends ApiController {
    public function index(Product $product) {
        $categories = $product->categories;

        return $this->showAll($categories);
    }


    public function update(Request $request, Product $product, Category $category) {
        /**
         * syncWithoutDetaching() method on the categories() relationship of the $product instance to add the given category to the product's categories without detaching any existing categories.
         */
        $product->categories()->syncWithoutDetaching($category->id);

        $category = $product->categories;

        return $this->showAll($category);
    }


    public function destroy(Product $product, Category $category) {
        /**
         * It first checks if the given category is associated with the product. If not, it returns an error response with a message and a status code of 404 (Not Found).
         */
        if (!$product->categories()->find($category->id)) {
            return $this->errorResponse("Product is not associated with the given category", 404);
        }

        /**
         * if the category is associated with the product, it uses the detach() method on the categories() relationship of the $product instance to remove the given category.
         */
        $product->categories()->detach([$category->id]);

        $categories = $product->categories;

        return $this->showAll($categories);
    }


}
