<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductBuyerTransactionController extends ApiController {
    public function store(Request $request, Product $product, User $buyer) {
        $rules = [
            'quantity' => 'required|integer|min:1',
        ];


        $this->validate($request, $rules);

        // BUYER ID AND SELLER ID SHOULD NOT BE SAME ,SELLER CANNOT PURCHASE THEIR OWN PRODUCT
        if ($buyer->id === $product->seller->id) {
            return $this->errorResponse('Wooahh!! we caught for increased ur sales number', 409);
        }

        // PRODUCT IS AVAILABLE OR NOT
        if (!$product->isAvailable()) {
            return $this->errorResponse('Product is not available', 409);
        }

        // if Requested quantity is greater than the available quantity of the product i.e insufficient quantity available, an error response is returned.
        if ($request->quantity > $product->quantity) {
            return $this->errorResponse('We do not have sufficient quantity', 409);
        }

        // checks if the buyer is verified
        if (!$buyer->isVerified()) {
            return $this->errorResponse('You must be verified to purchase on our portal', 409);
        }

        // checks if the seller of the product is verified
        if (!$product->seller->isVerified()) {
            return $this->errorResponse('Seller is not verified user!, we cannot proceed with this transaction', 409);
        }

        /**
         * DB::transaction() method to handle the database transaction, ensuring data integrity.
         * decrement() method decrement the product quantity
         * Transaction::create() method create transaction
         */
        $transaction = DB::transaction(function () use ($product, $request, $buyer) {
            $product->decrement('quantity', $request->quantity);

            $transaction = Transaction::create([
                'quantity' => $request->quantity,
                'product_id' => $product->id,
                'buyer_id' => $buyer->id,
            ]);

            return $transaction;
        });

        return $this->showOne($transaction);
    }
}
