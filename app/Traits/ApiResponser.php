<?php
namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

trait ApiResponser
{
    private function successResponse($data , $code) {
        return response()->json($data, $code);
    }


    public function errorResponse($message , $code) {
        return response()->json(['error' => $message , 'code' => $code] , $code);
    }


    public function showAll(Collection $collection , int $code = 200) {

        if($collection->isEmpty()) {
            return $this->successResponse(['data' => $collection], $code);
        }

        /**
         * $collection->first()->transformer ==> It will give the collection ka transformer
         */
        $transformer = $collection->first()->transformer;

        $data = $this->transformData($collection, $transformer);

        return $this->successResponse($data, $code);
    }

    
    public function showOne(Model $model, $code = 200) {
        if(empty($model->toArray())) {
            return $this->successResponse(['data' => $model], $code);     
        }

        /**
         * retrieves the transformer from the model 
         */
        $transformer = $model->transformer;

        $data = $this->transformData($model, $transformer);

        return $this->successResponse($data, $code);
    }

    public function showMessage($message, $code = 200) {
        return response()->json(['message' => $message , 'code' => $code] , $code);
    }

    private function transformData($data, $transformer):array {
        /**
         * fractal():  to transform the data using the provided transformer.
         */
        $transformedCollection = fractal($data, new $transformer);
        return $transformedCollection->toArray();
    }
}
