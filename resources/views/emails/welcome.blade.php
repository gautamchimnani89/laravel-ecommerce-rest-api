<x-mail::message>
# Hello {{$user->name}}

Thank you for creating account on our platform! Please verify your account using the following link!

<x-mail::button :url=$verificationURL>
Button Text
</x-mail::button>

Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
